package com.hermes.lucene;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;

public class LuceneSearchFiles {

    String indexPathOne = "index/CQindex1-01";
    String indexPathTwo = "index/CQindex1-02";
    String indexPathThree = "index/CQindex1-03";

    String field = "contents";

    IndexReader indexReader;
    IndexSearcher searcher;
    QueryParser queryParser;
    StandardAnalyzer analyzer;
    Query query;
    TopDocs topDocs;
    ScoreDoc[] docIds;

    HashMap<String, Integer> fieldResultMap = new HashMap<String, Integer>();



    public HashMap<String, Integer> getFieldValues(String queryString) throws IOException {

        indexReader = DirectoryReader.open(FSDirectory.open(Paths.get("index/CQindex1-01")));
        searcher = new IndexSearcher(indexReader);
        analyzer = new StandardAnalyzer();
        queryParser = new QueryParser(field, analyzer);
        try {
            query = queryParser.parse(queryString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        topDocs = searcher.search(query, 10);
        docIds = topDocs.scoreDocs;


        for (int i = 0; i<docIds.length; i++) {
            Document document = indexReader.document(docIds[i].doc);
            fieldResultMap.put(document.getField("TitleCN").stringValue(), docIds[i].doc);
        }

        return fieldResultMap;

    }
}
