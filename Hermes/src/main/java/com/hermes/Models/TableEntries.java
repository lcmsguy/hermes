package com.hermes.Models;

import javafx.beans.property.SimpleStringProperty;

public class TableEntries {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TableEntries(String value) {

        this.value = value;
    }
}
