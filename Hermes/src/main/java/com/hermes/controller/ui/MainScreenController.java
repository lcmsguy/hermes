package com.hermes.controller.ui;

import com.hermes.Models.TableEntries;
import com.hermes.lucene.LuceneSearchFiles;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class MainScreenController implements Initializable {
    public TextField titleCNTextField;
    public TextField titleENTextField;
    public TextField authorsCNTextField;
    public TextField authorsENTextField;
    public TextField orgsCNTextField;
    public TextField orgsENTextField;
    public TextField journalInfoCNTextField;
    public TextField journalInfoENTextField;
    public TextField fundCNTextField;
    public TextField fundENTextField;
    public TextField abstractCNTextField;
    public TextField abstractENTextField;
    public TextField keywordCNTextField;
    public TextField keywordENTextField;
    public TextField pathTextField;
    public TextField authorInformationTextField;

    public RadioButton titleCNRadioButton;
    public RadioButton titleENRadioButton;
    public RadioButton authorsCNRadioButton;
    public RadioButton authorsENRadioButton;
    public RadioButton orgsCNRadioButton;
    public RadioButton orgsENRadioButton;
    public RadioButton journalInfoCNRadioButton;
    public RadioButton journalInfoENRadioButton;
    public RadioButton fundCNRadioButton;
    public RadioButton fundENRadioButton;
    public RadioButton abstractCNRadioButton;
    public RadioButton abstractENRadioButton;
    public RadioButton keywordCNRadioButton;
    public RadioButton keywordENRadioButton;
    public RadioButton authorInformationRadioButton;
    public RadioButton pathRadioButton;

    public Button searchButton;

    public ToggleGroup fieldsSelection;

    public RadioButton selectedRadioButton;

    public TableView<TableEntries> tableView;

    public AnchorPane currentPane;

    HashMap<String, Integer> fieldResultMap = new HashMap<>();


    LuceneSearchFiles luceneSearchFiles = new LuceneSearchFiles();

    private HashMap<RadioButton, TextField> textFieldRadioButtonMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentPane = (AnchorPane) searchButton.getParent();
        this.initiateMap();
        selectedRadioButton = (RadioButton) fieldsSelection.getSelectedToggle();
    }

    public void searchButtonPressed() {
        String queryString = textFieldRadioButtonMap.get(selectedRadioButton).getText();
        try {
            fieldResultMap = luceneSearchFiles.getFieldValues( "TitleCN:" + queryString);

            if(fieldResultMap != null){
                tableView = getTableView();
                TableColumn<TableEntries, String> tableColumn = new TableColumn<>("TitleCN");
                tableColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
                tableView.setItems(getTableEntries(fieldResultMap));
                tableView.getColumns().add(tableColumn);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void radioButtonChanged() {
        selectedRadioButton = (RadioButton) fieldsSelection.getSelectedToggle();
    }

    private void initiateMap(){
        textFieldRadioButtonMap.put(titleCNRadioButton, titleCNTextField);
        textFieldRadioButtonMap.put(titleENRadioButton, titleENTextField);
        textFieldRadioButtonMap.put(authorsCNRadioButton, authorsCNTextField);
        textFieldRadioButtonMap.put(authorsENRadioButton, authorsENTextField);
        textFieldRadioButtonMap.put(orgsCNRadioButton, orgsCNTextField);
        textFieldRadioButtonMap.put(orgsENRadioButton, orgsENTextField);
        textFieldRadioButtonMap.put(journalInfoCNRadioButton, journalInfoCNTextField);
        textFieldRadioButtonMap.put(journalInfoENRadioButton, journalInfoENTextField);
        textFieldRadioButtonMap.put(fundCNRadioButton, fundCNTextField);
        textFieldRadioButtonMap.put(fundENRadioButton, fundENTextField);
        textFieldRadioButtonMap.put(abstractCNRadioButton, abstractCNTextField);
        textFieldRadioButtonMap.put(abstractENRadioButton, abstractENTextField);
        textFieldRadioButtonMap.put(keywordCNRadioButton, keywordCNTextField);
        textFieldRadioButtonMap.put(keywordENRadioButton, keywordENTextField);
        textFieldRadioButtonMap.put(authorInformationRadioButton, authorInformationTextField);
        textFieldRadioButtonMap.put(pathRadioButton, pathTextField);
    }

    private TableView<TableEntries>  getTableView(){
        tableView = new TableView<>();
        tableView.setLayoutX(465);
        tableView.setLayoutY(30);
        tableView.setMaxHeight(582);
        tableView.setMinWidth(371);
        currentPane.getChildren().add(tableView);

        tableView.setRowFactory(tv -> {
            TableRow<TableEntries> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    TableEntries rowData = row.getItem();
                    System.out.println("Double click on: "+rowData.getValue());
                }
            });
            return row ;
        });

        return tableView;

    }
    private ObservableList<TableEntries> getTableEntries(HashMap<String,Integer> fieldResultMap){

        ObservableList<TableEntries> tableEntries = FXCollections.observableArrayList();
        List<String> list = new ArrayList<>(fieldResultMap.keySet());

        for (int i = 0; i< list.size() ; i++){
            tableEntries.add(new TableEntries(list.get(i)));
        }

        return tableEntries;
    }
}
